module.exports = function(grunt) {
    
    var DEV_PATH            = 'app/',
        DIST_PATH           = 'www/';
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! \n' +
' * FLOW: TheCraftLibrary  \n' +
' * @package   Portfolio\n' +
' * @author    Florian Fievet\n' +
' * @copyright <%= grunt.template.today("yyyy") %> FLOW',
        
        sprite:{
            desktop: {
                algorithm: 'top-down',
                src: DEV_PATH+'sprites/*.png',
                destImg: DEV_PATH+'img/sprites.png',
                destCSS: DEV_PATH+'css/sprites.css'
            }
        },
        
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7,
                    cache: false
                },
                files: [
                    {
                        expand: true,
                        cache: false,
                        cwd: DEV_PATH+'img/',
                        src: ['**/*.png'],
                        dest: DIST_PATH+'img/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        expand: true,
                        cache: false,
                        cwd: DEV_PATH+'img/',
                        src: ['**/*.jpg'],
                        dest: DIST_PATH+'img/',
                        ext: '.jpg'
                    }
                ]
            }
        },
        
        copy: {
            assets: {
                files: [
                    {expand: true, flatten: true, src: [DEV_PATH+'i18n/*'],         dest: DIST_PATH+'i18n', filter: 'isFile'}
                ]
            }
        },
        
        compass: {
            desktop: {
                options: {
                    sassDir: DEV_PATH+'sass/',
                    specify: DEV_PATH+'sass/app.scss',
                    cssDir: DIST_PATH+'css/'
                }
            }
        },

        browserify: {
            dist: {
                options: {
                    transform: [["babelify", {presets: ["es2015", "react", "stage-0"]}]]
                },
                src: ['app/js/client.jsx'],
                dest: 'www/js/app.build.js'
            }
        },
        
        concat: {
            css: {
                src: [
                    DEV_PATH+'css/*.css'
                ],
                dest: DIST_PATH+'css/styles.css'
            }
        },
        
        jshint: {
            files: [DEV_PATH+'js/*.js']
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                expand: true,
                cwd: DIST_PATH,
                src: ['css/app.css'],
                dest: DIST_PATH,
                ext: '.min.css'
            }
        },
        
        watch: {
            files: [DEV_PATH+'sass/*.scss', DEV_PATH+'js/*.js'],
            tasks: ['compass', 'concat:js', 'concat:css'],
            options: {
                debounceDelay: 250
            }
        }
    });

    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-browserify');


    grunt.registerTask('dev', [
        'watch'
    ]);
    
    
    grunt.registerTask('default', [
        /*'compass:desktop',*/
        'browserify'/*,
        'cssmin',
        'concat:css'*/
    ]);
    
    grunt.registerTask('build', [
        'sprite:desktop',
        'concat:sass',
        'compass:desktop',
        'concat:js',
        'concat:css',
        'copy:assets',
        'imagemin'
    ]);
    
};