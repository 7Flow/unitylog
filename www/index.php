<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9 lte9"> <![endif]-->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Unity Live Logger</title>
    <meta name="author" content="Florian Fievet - flow.devilicious-games.com">
    <meta name="description" content="Développeur front-end - Unity">
    <meta name="keywords" content="unity 3d live logger">
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/app.css">

    <!--[if IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <script src="js/modernizr.custom.js"></script>
</head>
<body>

    <main id="app"></main>

    <script type='text/javascript' src='js/all.js'></script>
</body>
</html>