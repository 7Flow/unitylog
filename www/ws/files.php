<?php
// set default timezone
date_default_timezone_set('Europe/Berlin');

// get all reports .txt
$dir = new DirectoryIterator( '../'.$_GET['folder'].'/' );
$results = [];
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot() && strrpos($fileinfo->getFilename(), '.txt')) {
        $file = new stdClass();
        $file->id = $fileinfo->getCTime();
        $file->name = $fileinfo->getFilename();
        $file->date = date('d M Y H:i:s', $fileinfo->getMTime());
        $file->path = $fileinfo->getPathname();
        array_push( $results, $file );
    }
}
header('Content-Type: application/json');
echo( json_encode($results) );
?>