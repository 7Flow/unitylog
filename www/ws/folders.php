<?php
// set default timezone
date_default_timezone_set('Europe/Berlin');

// get all folders
$paths = array_filter( glob('../reports/*', GLOB_ONLYDIR) );

$results = [];
for ($i=0; $i<count($paths); ++$i) {
    $folder = new stdClass();
    $folder->name = str_replace('../reports/', '', $paths[$i]);
    $folder->path = $paths[$i];

    array_push( $results, $folder );
}

header('Content-Type: application/json');
echo( json_encode($results) );
?>