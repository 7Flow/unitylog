<?php
$folder = "reports/".$_POST["folder"];

if (!file_exists($folder)) {
   mkdir($folder, 0755);
}

$file = $folder."/report_" . $_POST["session"] . ".txt";

$fp = fopen($file, "w") or die("Couldn't open $file for writing!");
fwrite($fp, $_POST["log"]) or die("Couldn't write values to file!");

fclose($fp);
echo "Saved to $file successfully!";
?>