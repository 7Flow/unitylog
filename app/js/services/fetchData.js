import fetch from 'isomorphic-fetch'
import base64 from 'base-64'

const meta = {
    'Content-Type': 'application/json',
    'Accept-Encoding': 'gzip'
};

export function fetchData( url, query, vars, credentials ) {
    let _data = {
        method: 'POST',
        headers: new Headers(meta)
    };
    _data.body = JSON.stringify( {query: query, variables: vars || null} );

    if (credentials) {
        _data.headers.Authorization = 'Basic '+ base64.encode(credentials);
        _data.credentials = 'same-origin';
    }

    return fetch( url, _data );
}
