import { fetchData } from '../services/fetchData'

export const USER_FETCHING = 'U_FETCH'
export const USER_RECEIVE = 'U_RECEIVE'


export function fetchUser( dispatch, path, credentials ) {
    dispatch({type: USER_FETCHING});

    fetchData( path, credentials )
        .then(response => response.json())
        .then(data => {

            dispatch({
                type: USER_RECEIVE,
                data: data.data.users
            })
        });
}
