import { fetchData } from '../services/fetchData'

export const BROWSE_FETCHING = 'B_FETCH'
export const BROWSE_RECEIVE = 'B_RECEIVE'

export function fetchContent( dispatch, path, query, vars, credentials ) {
    dispatch({type: BROWSE_FETCHING});

    fetchData( path, query, vars, credentials )
        .then(response => response.json())
        .then(data => {
            dispatch({
                type: BROWSE_RECEIVE,
                data: data.data.folders
            })
        });
}
