import { fetchData } from '../services/fetchData'

export const AUTH_LOGGED_IN  = 'AUTH_LOGIN'
export const AUTH_SIGNED_IN  = 'AUTH_SIGNIN'
export const AUTH_LOGGED_OUT = 'AUTH_LOGOUT'
export const AUTH_FAILED     = 'AUTH_FAILED'

export function authenticate( dispatch, path, query, vars, credentials ) {
    fetchData( path, query, vars, credentials )
        .then(response => response.json())
        .then(data => {
            if (data && data.data.auth) {
                dispatch({
                    type: AUTH_LOGGED_IN,
                    data: data.data.auth
                })
            } else {
                dispatch({
                    type: AUTH_FAILED,
                    data: data.data.auth
                })
            }
        });
}
