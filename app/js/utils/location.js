
export function parseQueryString( querystring )
{
    var objURL = {};
    querystring.replace(
        /([^?=&\s]+)=([^&\s]*)/g,
        function( $0, $1, $2, $3 ) {
            objURL[ $1 ] = $2;
        }
    );
    return objURL;
}