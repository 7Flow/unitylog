import React from 'react'
import { render } from 'react-dom'

class AbstractPage extends React.Component
{
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return(
            <section></section>
        )
    }
}

export default AbstractPage;