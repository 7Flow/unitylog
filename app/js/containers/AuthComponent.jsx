import React from 'react';
import {connect} from 'react-redux';


/**
 * Transform a basic Component into an Auth-required Component
 * If User is not logged in, redirect to the Login route
 * @param Component
 */
export function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth();
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth();
        }

        checkAuth() {
            if (!this.props.auth || !this.props.auth.logged) {
                let redirectAfterLogin = this.props.location.pathname;
                this.props.history.push( '/login?redirectTo='+redirectAfterLogin );
            }
        }

        render() {
            return (
                <div>
                {this.props.auth.logged === true
                    ? <Component {...this.props}/>
                    : null
                }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        auth: state.auth
    });

    return connect(mapStateToProps)(AuthenticatedComponent);
}