import React from 'react'
import { render } from 'react-dom'

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Helmet }     from "react-helmet";

import PropTypes from 'prop-types'

import AbstractPage from './AbstractPage.jsx'

import { NOT_LOADED, LOADING, LOADED } from '../actions/generic'
import { fetchContent } from '../actions/browse'


class Folder extends AbstractPage {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            path: '/'
        }

        this.queries = {
            content: `
            query content($organization: Int!, $parent: Int) {
                folders(fk_organization: $organization, fk_parent: $parent) {
                    name
                }
            }`
        }
    }

    componentDidMount()
    {
        console.log(':didMount');
        let vars = {
            organization: 1,
            parent: null
        }
        this.props.fetchContent( 'http://localhost:4000/api', this.queries.content, vars );
    }

    getRouteName()
    {
        return this.props.params.folderName;
    }

    seeReport( fileName )
    {
        this.context.router.push( '/'+ this.props.params.folderName +'/'+ fileName );
    }

    render() {
        console.log('[Browse]:display content of ');
        console.log( this.props );

        return (
            <div id="browser" className="folder">
                    <Helmet
                        title="Browse"
                    />
                    <div className="grid">
                    {this.props.items.map( (file) => {
                        return (
                            <div className="row display" key={file.id}>
                                <div className="columns">
                                    <div className="content">
                                        <h4>{file.name}</h4>
                                        <time>{file.date}</time>
                                        <details onClick={this.seeReport.bind(this, file.name)}>
                                            <summary className="expanded button large">Voir</summary>
                                            {this.props.children && this.props.children.props.params.fileName == file.name && this.props.children}
                                        </details>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                    </div>
            </div>
        );
    }
}


Folder.propTypes = {
    itemCount: PropTypes.number,
    items: PropTypes.array
};

Folder.defaultProps = {
    itemCount: 0,
    items: []
};


const mapStateToProps = (state, ownProps) => {
    return {
        itemCount:  state.browse.itemCount,
        items:      state.browse.items
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchContent: fetchContent.bind(null, dispatch)
    }
};

export default withRouter( connect(mapStateToProps, mapDispatchToProps)(Folder) );