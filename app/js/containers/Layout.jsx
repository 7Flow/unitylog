import React, { Component } from 'react'
import { ConnectedRouter }  from 'react-router-redux'
import { Route, Switch }    from 'react-router-dom'

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Helmet }   from "react-helmet";

import Header       from '../components/Header.jsx'
import Breadcrumbs  from '../components/Breadcrumbs.jsx'
import Footer       from '../components/Footer.jsx'

import containers   from './containers.jsx'
import NoMatch      from './NoMatch.jsx'

import {requireAuthentication} from './AuthComponent.jsx'

/**
 *
 */
class Layout extends Component
{
    render()
    {
        return (
            <div>
                <Helmet
                    htmlAttributes={{lang: "en", amp: undefined}} // amp takes no value
                    titleTemplate="%s | React App"
                    titleAttributes={{itemprop: "name", lang: "en"}}
                    meta={[
                        {name: "description", content: "Server side rendering example"},
                        {name: "viewport", content: "width=device-width, initial-scale=1"},
                    ]}
                />
                <Header />
                <Breadcrumbs routes={this.props.route} router={this.props.router} location={this.props.location} />
                <Switch>
                    {this.props.routes.map( (page, index) => {
                        if (containers.hasOwnProperty(page.component)) {
                            let PageComponent;
                            if (page.authRequired) {
                                PageComponent = requireAuthentication( containers[page.component] );
                            } else {
                                PageComponent = containers[page.component];
                            }
                            return (<Route exact key={index} path={page.url} render={() => (
                                <PageComponent {...this.props} />
                            )} />);
                        } else {
                            return console.warn('[Layout]: missing component "'+page.component+'" defined by routes');
                        }
                    })}
                    <Route render={() => (
                        <NoMatch {...this.props} />
                    )}  />
                    {this.props.children}
                </Switch>
                <Footer />
            </div>
        )
    }
}

Layout.propTypes = {

};

Layout.defaultProps = {

};

const mapStateToProps = (state, ownProps) => {
    // inject into state the props passed by the server
    console.log( ownProps );
    if (ownProps.routes) {
        state.routes = ownProps.routes;
    }

    return {
        routes: state.routes,
        router: state.router
    }
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

export default withRouter( connect(mapStateToProps, mapDispatchToProps)(Layout) );