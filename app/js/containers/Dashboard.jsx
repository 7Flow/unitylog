import React from 'react'
import { render } from 'react-dom'

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Helmet }     from "react-helmet";

import PropTypes from 'prop-types'

import AbstractPage from './AbstractPage.jsx'

import { NOT_LOADED, LOADING, LOADED } from '../actions/generic'
import { fetchContent } from '../actions/browse'


class Dashboard extends AbstractPage {

    constructor(props) {
        super(props);
    }

    componentDidMount()
    {

    }

    render()
    {
        return (
            <div id="dashboard" className="dashboard">
                <Helmet
                    title="Welcome to our Homepage"
                />
            </div>
        )
    }
}

Dashboard.propTypes = {

};

Dashboard.defaultProps = {

};


const mapStateToProps = (state, ownProps) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    }
};

export default withRouter( connect(mapStateToProps, mapDispatchToProps)(Dashboard) );