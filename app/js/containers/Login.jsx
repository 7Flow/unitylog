import React            from 'react'
import { render }       from 'react-dom'

import { withRouter }   from 'react-router-dom'
import { connect }      from 'react-redux'
import { Helmet }       from 'react-helmet'

import PropTypes        from 'prop-types'

import AbstractPage     from './AbstractPage.jsx'

import RaisedButton     from 'material-ui/RaisedButton'
import { ValidatorForm } from 'react-form-validator-core'

import SmartTextFieldValidator    from '../components/form/SmartTextFieldValidator.jsx'

import { authenticate } from '../actions/auth'

import { parseQueryString } from '../utils/location'


class Login extends AbstractPage
{
    constructor(props) {
        super(props);

        this.queries = {
            content: `
            query content($email: String!, $password: String!) {
                auth(email: $email, password: $password) {
                    id
                    firstName
                    lastName
                    avatar
                    fk_organization
                }
            }`
        }

        this.validate = this.validate.bind(this);
        this.onError = this.onError.bind(this);
    }

    componentWillReceiveProps( nextProps )
    {
        if (nextProps.auth.logged) {
            let _queryParams = parseQueryString( this.props.location.search );
            if (_queryParams.hasOwnProperty('redirectTo')) {
                this.props.history.push( _queryParams['redirectTo'] );
            } else {
                this.props.history.push( '/dashboard' );
            }
        }
    }

    onError( errors )
    {
        console.log( errors );
    }

    validate(e)
    {
        let vars = {
            email: document.getElementById('email').value,
            password: document.getElementById('pwd').value
        }
        this.props.authenticate( 'http://localhost:4000/api', this.queries.content, vars );
    }

    render()
    {
        return (
            <div id="login" className="login">
                <Helmet
                    title="Login"
                />
                <ValidatorForm ref="form"
                    onSubmit={this.validate}
                    onError={this.onError}
                    instantValidate={false}>
                    <SmartTextFieldValidator
                        id="email"
                        name="email"
                        hintText="Email"
                        type="email"
                        validators={['required', 'isEmail']}
                        errorMessages={['this field is required', 'email is not valid']}
                    />
                    <SmartTextFieldValidator
                        id="pwd"
                        name="password"
                        hintText="Password"
                        type="password"
                        validators={['required']}
                        errorMessages={['this field is required', 'password must be at least 3 characters long']}
                    />
                    <RaisedButton
                        label="Sign in"
                        primary={true}
                        type="submit"
                    />
                </ValidatorForm>
            </div>
        )
    }
}

Login.propTypes = {

};

Login.defaultProps = {

};


const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.auth
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        authenticate: authenticate.bind(null, dispatch)
    }
};

export default withRouter( connect(mapStateToProps, mapDispatchToProps)(Login) );