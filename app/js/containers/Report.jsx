import React from 'react'
import { render } from 'react-dom'

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import PropTypes from 'prop-types'

import AbstractPage from './AbstractPage.jsx'


class Report extends AbstractPage
{
    constructor(props) {
        super(props);

        this.state = {
            data: '',
            logChecked: false,
            warningChecked: false,
            errorChecked: true
        }
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount()
    {
        var _fileName = '/reports/'+this.props.params.folderName +'/'+ this.props.params.fileName;
        this.serverRequest = $.get(_fileName, (result) => {
            this.setState({
                data: result
            });
        });
    }

    getRouteName()
    {
        return "Log"
    }

    confirmDelete( id ) {
        console.log('delete');
        //this.context.router.push( id );
    }

    onChange(e)
    {
        console.log("Filter");
        console.log( e );
        console.log(  );
        console.log(  );

        let _name = e.target.getAttribute("name") + 'Checked';
        let _state = {};
        _state[_name] = e.target.checked;

        this.setState( _state );
    }

    onSubmit(e)
    {
        console.log("Submit");
        console.log( e );
    }

    /**
     *  - type [Log | Warning | Exception | Error]
     *  - time (from startup, ms)
     *  - condition
     *  - stacktrace
     */
    buildLogs( data )
    {
        if (data == '' || !data.length) return;

        let _logs = data.split('\n\r\n\r');
        let _details = [];

        for (let i=0; i<_logs.length; ++i) {
            let _log = _logs[i].split(';;');
            let _logType = _log[0].toLowerCase();
            _details.push(
                <details className={"display " + _logType} key={i}>
                    <summary className="callout">{_log[0]}: {_log[2]}</summary>
                    <code>{_log[3]}</code>
                    <time>{_log[1]}</time>
                </details>
            )
        }
        return _details
    }

    render() {
        console.log('[Report]:render');
        if (!this.state.data) this.state.data = '';

        return (
            <div className={"logs " + (this.state.logChecked ? 'log-visible ' : ' ') + (this.state.warningChecked ? 'warning-visible ' : ' ') + (this.state.errorChecked ? 'error-visible ' : ' ')}>
                <form onSubmit={this.onSubmit}>
                    <ul className="filters">
                        <li>
                            Filtres :
                        </li>
                        <li>
                            <input type="checkbox" name="log" onChange={this.onChange} checked={this.state.logChecked} />
                            <label htmlFor="log">Log</label>
                        </li>
                        <li>
                            <input type="checkbox" name="warning" onChange={this.onChange} checked={this.state.warningChecked} />
                            <label htmlFor="warning">Warning</label>
                        </li>
                        <li>
                            <input type="checkbox" name="error" onChange={this.onChange} checked={this.state.errorChecked} />
                            <label htmlFor="error">Error</label>
                        </li>
                    </ul>
                </form>

                {this.buildLogs(this.state.data)}
                <button type="button" className="button expanded large alert" onClick={this.confirmDelete}>Supprimer</button>
            </div>
        )
    }
}

export default Report