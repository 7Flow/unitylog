import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import PropTypes from 'prop-types';

class NoMatch extends Component {

    componentWillMount() {

    }

    render() {
        return (
            <div id="noMatch" className="debug">
                <p>{this.props.baseUrl} -- {this.props.location.pathname}</p>
                <p>{this.props.routes && JSON.stringify(this.props.routes)}</p>
            </div>
        );
    }
}

export default withRouter(NoMatch);