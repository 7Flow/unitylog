import Home     from './Home.jsx'
import Login    from './Login.jsx'
import Dashboard from './Dashboard.jsx'
import Browser  from './Browser.jsx'
import Report   from './Report.jsx'


export default {
    Home: Home,
    Login: Login,
    Dashboard: Dashboard,
    Browser: Browser,
    Report: Report
}