import React from 'react'
import { render } from 'react-dom'

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Helmet }     from "react-helmet";

import PropTypes from 'prop-types'

import AbstractPage from './AbstractPage.jsx'


class Home extends AbstractPage {

    constructor(props) {
        super(props);
    }

    componentDidMount()
    {

    }

    render()
    {
        return (
            <div id="home" className="home">
                <Helmet
                    title="Welcome to our Homepage"
                />
            </div>
        )
    }
}

Home.propTypes = {

};

Home.defaultProps = {

};


const mapStateToProps = (state, ownProps) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    }
};

export default withRouter( connect(mapStateToProps, mapDispatchToProps)(Home) );