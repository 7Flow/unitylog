
const initialState = typeof window != 'undefined' ? window.__initialState__.routes : [];

export function routes(state = initialState, action) {
    switch (action.type) {
        case 'routes':
            return action.data;
        default:
            return state;
    }
}
