import { AUTH_LOGGED_IN, AUTH_FAILED, AUTH_LOGGED_OUT } from '../actions/auth';

const initialState = {
    user: null,
    logged: false,
    token: null
}

export function auth(state = initialState, action) {
    switch (action.type) {
        case AUTH_LOGGED_IN:
            return { data: action.data, logged: true };

        case AUTH_LOGGED_OUT:
            return initialState;

        default:
            return state;
    }
}