import { BROWSE_FETCHING, BROWSE_RECEIVE } from '../actions/browse'
import { NOT_LOADED, LOADING, LOADED } from '../actions/generic'

const initialState = {
    itemCount: 0,
    itemLoadState: NOT_LOADED,
    items: [],
    selected: null
};

export function browse(state = initialState, action) {
    switch (action.type) {
        case BROWSE_FETCHING:
            return Object.assign({}, state, {
                itemLoadState: LOADING,
                itemCount: 0,
                items: []
            });

        case BROWSE_RECEIVE:
            return Object.assign({}, state, {
                itemLoadState: LOADED,
                items: action.data,
                itemCount: action.data.length
            });

        default:
            return state;
    }
}
