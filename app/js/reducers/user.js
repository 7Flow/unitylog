import { USER_FETCHING, USER_RECEIVE } from '../actions/user'

const initialState = {

}

export function user(state = initialState, action) {
    switch (action.type) {
        case USER_FETCHING:
            return Object.assign({}, state, {

            });

        case USER_RECEIVE:
            return Object.assign({}, state, {

            });
        default:
            return state
    }
}