import React from 'react'
import ReactDOM from 'react-dom'

import App from './App.jsx'

const element = document.getElementById('app');

ReactDOM.render(
    <App />,
    document.getElementById('app')
);


if (process.env.NODE_ENV !== 'production') {
    window.React = React;
}