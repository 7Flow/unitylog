import React from 'react'
import { render } from 'react-dom'

class Footer extends React.Component
{
    render() {
        return(
            <footer>
                <span>© 2016 Florian Fievet - Tous droits réservés</span>
            </footer>
        );
    }
}

export default Footer;