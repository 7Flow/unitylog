import React from 'react'
import { render } from 'react-dom'

import { Router, Route, Link, browserHistory, hashHistory, useRouterHistory, IndexRoute } from 'react-router'


class Header extends React.Component
{
    render()
    {
        return (
            <header>
                <div className="top-bar">
                    <div className="top-bar-left">
                        <h1>Unity Error Log</h1>
                    </div>

                    <div className="top-bar-right">
                        <ul className="menu">
                            <li>
                                <input type="search" placeholder="Search" />
                            </li>
                            <li>
                                <button type="button" className="button">Search</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;