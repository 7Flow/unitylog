/**
 * Simple bread crumb, works with simple url, and simple hierarchy
 *  @requires react
 *  @requires react-router
 */
import React from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, browserHistory, hashHistory, useRouterHistory, IndexRoute } from 'react-router'


class Breadcrumbs extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    /**
     * Get the displayed name of the root.
     * Could be:
     * - the component name
     * - the part of the location (according to the depth)
     * @param route A react-router route
     */
    getRoutName( route, depth )
    {
        // home has no path, by default
        let _path = this.props.location.pathname.split('/');

        /*
        if (_path[depth]) {
            return _path[depth];
        } else if (depth==0) {
            return route.indexRoute ? route.indexRoute.component.name : route.component.name;
        } else {
            return undefined;
        }
        */
    }

    buildLink( route, list, depth )
    {
        let _name = this.getRoutName(route, depth);
        if (!_name) return;

        let _path = _name == 'Home' ? '#/' : '#/'+_name;

        // is this route active
        if (this.props.router.isActive(route)) {
            // is the route the last index?
            list.push( <li key={depth}><a href={_path}>{_name}</a></li> )
            if (route.childRoutes) {
                this.buildLink( route.childRoutes[0], list, depth+1 )
            }
        }
    }

    buildList( routes )
    {
        let _links = [];
        this.buildLink( routes, _links, 0 )
        return _links
    }

    render()
    {
        return (
            <ul className="row breadcrumb">
                {this.buildList( this.props.routes )}
            </ul>
        )
    }
}

export default Breadcrumbs;