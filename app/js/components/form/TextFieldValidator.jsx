/* eslint-disable */
import React from 'react';
import TextField from 'material-ui/TextField';
/* eslint-enable */
import { ValidatorComponent } from 'react-form-validator-core';

import PropTypes from 'prop-types'

/**
 * Material UI Wrapper to add Validator option (use 'react-form-validator-core')
 * Add 'onBlurValidation' option
 */
export default class TextFieldValidator extends ValidatorComponent
{
    constructor(props)
    {
        super(props);
        this.handleBlur = this.handleBlur.bind(this);
    }

    handleBlur(event)
    {
        if (!this.instantValidate) this.validate( this.props.value );
    }

    render()
    {
        // eslint-disable-next-line
        const { errorMessages, validators, requiredError, errorText, validatorListener, ...rest } = this.props;

        return (
            <TextField
                {...rest}
                ref={(r) => { this.input = r; }}
                errorText={(!this.state.isValid && this.getErrorMessage()) || errorText}
                onBlur={this.handleBlur}
            />
        );
    }
}

TextFieldValidator.propTypes = {
    errorMessages: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.string,
    ]),
    validators: PropTypes.array,
    name: PropTypes.string,
    value: PropTypes.any,
    validatorListener: PropTypes.func,
};

TextFieldValidator.defaultProps = {
    errorMessages: 'error',
    validators: [],
    validatorListener: () => {},
};