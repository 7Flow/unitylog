/* eslint-disable */
import React from 'react'
import TextField from 'material-ui/TextField'
/* eslint-enable */
import TextFieldValidator from './TextFieldValidator.jsx'

import PropTypes from 'prop-types'

/**
 * Smart ValidatorComponent Wrapper
 * - ValidatorForm rely on input props value, so we need a wrapper around the input to update the props
 * So, automatically listen to input change to update props value
 */
export default class SmartTextFieldValidator extends React.Component
{
    constructor(props)
    {
        super(props);
        // avoid error 'A component is changing an uncontrolled input' if default value is undefined
        this.state = {
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event)
    {
        this.setState({
            value: event.target.value
        });
    }

    render()
    {
        return (
            <TextFieldValidator
                {...this.props}
                onChange={this.handleChange}
                value={this.state.value}
            />
        );
    }
}

SmartTextFieldValidator.propTypes = {
    errorMessages: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.string,
    ]),
    validators: PropTypes.array,
    name: PropTypes.string,
    value: PropTypes.any,
    validatorListener: PropTypes.func,
};

SmartTextFieldValidator.defaultProps = {
    errorMessages: 'error',
    validators: [],
    validatorListener: () => {},
};