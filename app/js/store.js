import { combineReducers, createStore, applyMiddleware  } from 'redux'
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';

/* Reducers */
import { routes } from  './reducers/routes'
import { auth } from    './reducers/auth'
import { browse } from  './reducers/browse'
import { user } from    './reducers/user'

const reducers = combineReducers({
    routes,
    auth,
    browse,
    user,
    router: routerReducer
});

function getInitialState() {
    // Grab the state from a global variable injected into the server-generated HTML
    let preloadedState = {}

    if (typeof window !== 'undefined' && window.__initialState__) {
        try {
            let data = JSON.parse( unescape(__initialState__) );
            /*for (let _key in data) {
             preloadedState[_key] = Immutable.fromJS( data[_key] );
             }*/
            preloadedState = data;
        } catch (e) {

        }

        delete window.__initialState__;
    }

    // Allow the passed state to be garbage-collected
    return preloadedState;
}

export function configureStore(history) {
    return createStore(
        reducers,
        applyMiddleware(
            createLogger(),
            routerMiddleware(history),
            thunk
        )
    );
}
