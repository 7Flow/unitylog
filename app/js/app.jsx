import React, { Component } from 'react'
import { Provider }         from 'react-redux'
import { ConnectedRouter }  from 'react-router-redux'

import createBrowserHistory from 'history/createBrowserHistory'
import createMemoryHistory  from 'history/createMemoryHistory'
import { configureStore }   from './store'

import Layout               from './containers/Layout.jsx'

import getMuiTheme          from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import {green100, green500, green700} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme(
    {
        palette: {
            primary1Color: green500,
            primary2Color: green700,
            primary3Color: green100,
        },
    },
    {
        avatar: {
            borderColor: null,
        },
        userAgent: false,
    }
);

export default class App extends Component
{
    render() {

        let history;
        if (process.env.hasOwnProperty('PATH')) {
            console.log('- - server - -');
            history = createMemoryHistory({
                basename: this.props.baseUrl || '',
                initialEntries: [this.props.location || '']
            })
        } else {
            console.log('- - client - -');
            history = createBrowserHistory({
                basename: this.props.baseUrl || ''
            })
        }

        return (
            <Provider store={configureStore(history)}>
                <ConnectedRouter history={history}>
                    <MuiThemeProvider muiTheme={muiTheme}>
                        <Layout {...this.props} />
                    </MuiThemeProvider>
                </ConnectedRouter>
            </Provider>
        )
    }
}