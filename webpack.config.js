path = require('path');
const webpack = require('webpack');

module.exports = {
    /*devtool: 'cheap-module-source-map',*/
    devtool: 'eval',
    entry: './app/js/app.jsx',
    output: {
        path: './www/js',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loaders: ['babel']
            }
        ]
    },
    plugins: [
        new webpack.PrefetchPlugin("react"),
        new webpack.PrefetchPlugin("react/lib/ReactComponentBrowserEnvironment")
    ]
};