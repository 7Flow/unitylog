import { Organization, Folder, User, Log } from './connectors';

const resolvers = {
    Query: {
        auth(root, args) {
            return User.findOne({ where: args });
        },
        users(root, args) {
            return User.findAll({ where: args });
        },
        user(root, args) {
            return User.findOne({ where: args });
        },
        organization(root, args) {
            return Organization.findOne({ where: args });
        },
        folders(root, args) {
            return Folder.findAll({ where: args });
        },
        folder(root, args) {
            return Folder.findOne({ where: args });
        }
    }
};

export default resolvers;