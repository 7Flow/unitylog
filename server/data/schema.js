import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import resolvers from './resolvers';

/* USE RESOLVERS INSTEAD OF STATIC MOCKS DATA
import mocks from '../mocks/mocks';
addMockFunctionsToSchema({ schema, mocks });
*/

const typeDefs = `
    type Query {
        users(fk_organization: Int): [User]
        user(id: Int): User

        organization(id: Int): Organization

        folders(fk_organization: Int, fk_parent: Int): [Folder]
        folder(id: Int): Folder

        auth(email: String, password: String): User
        signIn(firstName: String, lastName: String, email: String, password: String ): User
    }

    type Organization {
        id: Int
        name: String!
        creation: String!
    }
    type User {
        id: Int
        firstName: String!
        lastName: String!
        email: String!
        password: String!
        avatar: String
        creation: String!
        fk_organization: Int
    }
    type Folder {
        id: Int
        name: String!
        creation: String!
        fk_organization: Int
        fk_parent: Int
    }
    type Log {
        id: Int
        name: String!
        content: String!
        creation: String!
        fk_folder: Int
        fk_issue: Int
    }
    type Issue {
        id: Int
    }
`;

export default makeExecutableSchema({ typeDefs, resolvers });