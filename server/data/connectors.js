import Sequelize from 'sequelize';
import casual from 'casual';
import _ from 'lodash';


const db = new Sequelize('unityLiveLog', 'root', 'root', {
    host: 'localhost',
    port: 8889,
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

/**
 * Options to prevent Sequelize to:
 * - add plural to table's name
 * - add prefix on fields
 */
const defaultOptions = {
    freezeTableName: true
}

const OrganizationModel = db.define('organization', {
    id:             { type: Sequelize.INTEGER, primaryKey: true },
    name:           { type: Sequelize.STRING },
    creation:       { type: Sequelize.DATE },
}, defaultOptions);

const FolderModel = db.define('folder', {
    id:             { type: Sequelize.INTEGER, primaryKey: true },
    name:           { type: Sequelize.STRING },
    creation:       { type: Sequelize.DATE },
    fk_organization: { type: Sequelize.INTEGER },
    fk_parent:      { type: Sequelize.INTEGER, allowNull: true },
}, defaultOptions);

const UserModel = db.define('user', {
    id:             { type: Sequelize.INTEGER, primaryKey: true },
    firstName:      { type: Sequelize.STRING },
    lastName:       { type: Sequelize.STRING },
    email:          { type: Sequelize.STRING },
    password:       { type: Sequelize.STRING },
    avatar:         { type: Sequelize.STRING },
    creation:       { type: Sequelize.DATE },
    fk_organization: { type: Sequelize.INTEGER, allowNull: true },
}, defaultOptions);

const LogModel = db.define('log', {
    id:             { type: Sequelize.INTEGER, primaryKey: true },
    name:           { type: Sequelize.STRING },
    content:        { type: Sequelize.TEXT },
    creation:       { type: Sequelize.DATE },
}, defaultOptions);

UserModel.belongsTo( OrganizationModel,     { foreignKey: 'fk_organization', targetKey: 'id' });

FolderModel.belongsTo( FolderModel,         { foreignKey: 'fk_parent',      targetKey: 'id' });
FolderModel.belongsTo( OrganizationModel,   { foreignKey: 'fk_organization', targetKey: 'id' });

LogModel.belongsTo( FolderModel,            { foreignKey: 'fk_folder',   targetKey: 'id' });
LogModel.belongsTo( UserModel,              { foreignKey: 'fk_initiator', targetKey: 'id' });
LogModel.belongsTo( UserModel,              { foreignKey: 'fk_assignee', targetKey: 'id' });

const Organization = db.models.organization;
const Folder = db.models.folder;
const User = db.models.user;
const Log = db.models.log;

/**
 *
 */
db.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        db.sync().then( () => {
            console.log('Connection synced successfully.');
        });
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

export { Organization, Folder, User, Log };
