module.exports = {
    routes: [
        {
            url: "/",
            component: 'Home'
        },
        {
            url: "/login",
            component: 'Login'
        },
        {
            url: "/dashboard",
            component: 'Dashboard',
            authRequired: true
        },
        {
            url: "/browse/:folderSlug",
            component: 'Browser',
            authRequired: true
        },
        {
            url: "/users",
            component: 'Users',
            authRequired: true
        },
        {
            url: "/user/:id",
            component: 'User',
            authRequired: true
        }
    ],
    baseUrl: ''
};