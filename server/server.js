'use strict';

const path = require('path');
const fs = require('fs');

const Express = require('express');
const bodyParser = require('body-parser');
const graphql = require('express-graphql');
const consolidate = require('consolidate');

const app = new Express();

// PARSE X FORM ENCODED
app.use(bodyParser.urlencoded({extended: true}));
// PARSE JSON
app.use(bodyParser.json());

/**
 * Static assets server
 */
app.use('/css', Express.static( path.join(__dirname, '../www/css')));
app.use('/js', Express.static( path.join(__dirname, '../www/js')));
app.use('/assets', Express.static( path.join(__dirname, '../www/assets')));

/**
 * GraphQL Config
 */
import schema from './data/schema';
var root = {
    Query: () => {
        return 'Hello world!'
    }
};

app.use('/api', graphql({
    schema: schema,
    rootValue: root,
    graphiql: true
}));

/**
 * React Server-side
 */
const React = require('react');
const ReactDOMServer = require('react-dom/server');
import { Helmet } from 'react-helmet';

import App from '../app/js/App.jsx';
const routes = require('./routes');

app.get('*', function(req, res) {
    let props = Object.assign({}, routes, req.body);

    var content = ReactDOMServer.renderToString(
        React.createElement( App, props )
    );
    var _helmet = Helmet.renderStatic();

    return res.render("index.ejs", {
        helmet: _helmet,
        html: content,
        initialState: JSON.stringify(props)});
})

app.set('views', [path.join(__dirname, 'views')]);
app.engine('ejs', consolidate.ejs);

/**
 * Error handler
 */
app.use( function(err, req, res, next) {
    if(err) console.error(err.stack);
    next(err);
});

const env = process.env.NODE_ENV || 'dev';
// start the server
const port = env === 'production' ? 8080 : (process.env.PORT || 4000);

app.listen( port, (err) => {
    if (err) return console.error(err);
    console.info(`Server running on http://localhost:${port} [${env}]`);
} );
